var http = require("http");
var PORT = 8000;

var server = http.createServer(handler);
server.listen(PORT);
console.log("Server started @ localhost:" + PORT);

function handler(request, response) {
    console.log("[REQUEST] " + request.method + " " + request.url);
    response.writeHead(200);
    var fs = require("fs");
    var content = fs.readFileSync('pagina.html');
    response.write(content);
    response.end();
}