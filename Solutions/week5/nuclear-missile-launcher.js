//Our nuclear missile manager system is coded in Node.js.
//We are currently testing the system and it seams that the launchAll function does not work as expected.
//It should launch 5 missiles each 1 second apart. The current code tries to launch the missile #5 five times...

//Can you fix this for us? You know, it's pretty critical code...

//Note: There are 5 missiles labeled i which is a number in {0, 1, 2, 3, 4}. The missile i should be launched after i seconds.


function launchAll(numberOfMissiles) {

    let counter = 0

    let timerId;

        function callback() {
            console.log(`nuclear missile ${counter} was launched!`); 
            counter += 1;
            if(counter>=numberOfMissiles) {
                clearInterval(timerId);
            }
        }

        timerId = setInterval(callback, 1000)
    }

launchAll(3);


