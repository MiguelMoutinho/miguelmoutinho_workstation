var deathListFive = [
    { description: 'VERNITA GREEN', importance: 2 },
    { description: 'BILL', importance: 5 },
    { description: 'O REN ISHII', importance: 1 },
    { description: 'EllE DRIVER', importance: 4 },
    { description: 'BUDD', importance: 3 },

];

function sortByImportance(todos, orderType) {

    var orderedNames = []

    switch (orderType) {

        case 'asc':

            var orderedList = todos.sort(function (a, b) {
                if (a.importance > b.importance) {
                    return 1;
                }
                if (a.importance < b.importance) {
                    return -1;
                }
                return 0;
            });

            orderedList.forEach(function (item) { orderedNames.push(item.description) });
            return console.log("The bride's todo list: " + orderedNames);       

        case 'desc':

            var orderedList = todos.sort(function (a, b) {
                if (a.importance > b.importance) {
                    return -1;
                }
                if (a.importance < b.importance) {
                    return 1;
                }
                return 0;
            });

            orderedList.forEach(function (item) { orderedNames.push(item.description) });
            return console.log("The bride's todo list: " + orderedNames);

        default:
            return ('Please introduce asc or desc in the second argument of the function!!!')
    }
}


console.log(sortByImportance(deathListFive, 'desc'))