const http = require("http");
const fsPromises = require("fs").promises;
const path = require("path");
const PUBLIC_ROOT = "public"
const PORT = 8000;
const server = http.createServer(handler);
server.listen(PORT);
console.log("Server started @ localhost:" + PORT);

let mysql = require("mysql2/promise");

let connection = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'password',
  database: 'pokemon'
})

const typesOfPokemonImgs = [{ id: 7, type: "bug", img: "types/bug.png" }, { id: 17, type: "dark", img: "types/dark.png" }, { id: 16, type: "dragon", img: "types/dragon.png" }, { id: 13, type: "electric", img: "types/electric.png" }, { id: 18, type: "fairy", img: "types/fairy.png" }, { id: 2, type: "fighting", img: "types/fighting.png" }, { id: 10, type: "fire", img: "types/fire.png" }, { id: 3, type: "flying", img: "types/flying.png" }, { id: 8, type: "ghost", img: "types/ghost.png" }, { id: 12, type: "grass", img: "types/grass.png" }, { id: 5, type: "ground", img: "types/ground.png" }, { id: 15, type: "ice", img: "types/ice.png" }, { id: 1, type: "normal", img: "types/normal.png" }, { id: 4, type: "poison", img: "types/poison.png" }, { id: 14, type: "psychic", img: "types/psychic.png" }, { id: 6, type: "rock", img: "types/rock.png" }, { id: 9, type: "steel", img: "types/steel.png" }, { id: 11, type: "water", img: "types/water.png" }]



const routes = [{ method: "GET", path: /^\/deletePokemon/, handler: handlerDeletePokemon }, { method: "GET", path: /^\/deleteComment/, handler: handlerDeleteComment }, { method: "POST", path: /^\/formComments/, handler: handlePostCommentRequest }, { method: "POST", path: /^\/form/, handler: handlePostPokemon },
{ method: "POST", path: /^\/updatePokemon/, handler: handleUpdatePokemonPostRequest }, { method: "GET", path: /^\/getPokemonComments/, handler: handlerGetComments }, { method: "GET", path: /^\/getGeneration/, handler: handlerGetPokemonsByGen },
{ method: "GET", path: /^\/getPokemonByType/, handler: handlerGetPokemonType },
{ method: "GET", path: /^\/getPokemonInfoByID/, handler: handlerGetPokemonInfoById }, { method: "GET", path: /^\/getPokemonInfoByName/, handler: handlerGetPokemonInfoByName },
{ method: "GET", path: /\.*/, handler: handlerSPAPages }]

function handleChooser(method, url) {

  return routes.find((route) => route.method === method && route.path.test(url))
    .handler;
}


let extension = (data) => {
  let splittedData = data.split(".");
  const contentTypes = {
    gif: "image/gif",
    jpeg: "image/jpeg",
    jpg: "image/jpeg",
    png: "image/png",
    mp4: "video/mp4",
    html: "text/html",
    css: "text/css",
    js: "text/javascript",
  };
  return contentTypes[splittedData[1]];
};


async function handler(request, response) {

  const url = request.url;

  if (url === "/favicon.ico") {
    response.writeHead(200);
    return;
  }

  let selectedRoute = handleChooser(request.method, request.url);
  selectedRoute(request, response);
}

async function handlerSPAPages(request, response) {
  
  let url = request.url;

  if (url === "/favicon.ico") {
    response.writeHead(200);
    return;
  }
 
  if (!path.extname(url)) {
    url = "/index.html";
  }
  if (/^\/pokemonType/.test(url)) {
    url = url.slice(12)
  }
  if (/^\/updatePokemon/.test(url)) {
    url = url.slice(14)
  }
  else if (/^\/pokemonProfile/.test(url)) {
    url = url.slice(15)
  }
  else if (/^\/pokemon/.test(url)) {
    url = url.slice(8)
  }
  else if (/^\/generation/.test(url)) {
    url = url.slice(11)
  }
  const filePath = path.join(PUBLIC_ROOT, url)
  const type = extension(url);
  fsPromises
    .readFile(filePath)
    .then((data) => {
      response.writeHead(200, { "content-type": type });
      response.end(data);
    })
    .catch(() => {
      response.writeHead(404, { "content-type": "text/html" });
      response.end("ERROR! NOT FOUND!");
    });
}

async function handlerGetPokemonsByGen(request, response) {
  let url = request.url;
  console.log(url)
  let generation = url.slice(14, 15)
  console.log('generation-', generation)
  console.log('YOU ENTERED THIS HANDLER TO GET ALL POKEMONS BY GENERATION')

  const [data] = await connection.query(
    `SELECT * FROM pokemondata WHERE generation="${generation}"`
  );
  let JSONData = JSON.stringify(data)
  response.writeHead(200, { 'Content-Type': 'application/json' });
  response.end(JSONData);
}

async function handlerGetPokemonType(request, response) {
  console.log('YOU ENTERED THIS HANDLER TO GET ALL POKEMONS BY TYPE')
  console.log('requestURL', request.url)
  let typeId = (request.url).slice(18)
  let pokemonType = typesOfPokemonImgs.find((pokemon) => pokemon.id == typeId).type

  const [data] = await connection.query(
    `SELECT * FROM pokemondata WHERE type="${pokemonType}"`
  );
  let JSONData = JSON.stringify(data)
  response.writeHead(200, { 'Content-Type': 'application/json' });
  response.end(JSONData);
}

async function handlerGetPokemonInfoById(request, response) {
  console.log('YOU ENTERED THIS HANDLER TO GET POKEMON INFO BY ID')
  console.log('requestURL', request.url)
  let id = (request.url).slice(20)
  console.log(id)

  const [data] = await connection.query(
    `SELECT * FROM pokemondata WHERE id="${id}"`
  );
  let JSONData = JSON.stringify(data)
  response.writeHead(200, { 'Content-Type': 'application/json' });
  response.end(JSONData);
}

async function handlerGetPokemonInfoByName(request, response) {
  console.log('YOU ENTERED THIS HANDLER TO GET POKEMON BY NAME')
  console.log('REQUEST URL', request.url)
  let name = (request.url).slice(22)
  console.log(name)
  const [data] = await connection.query(
    `SELECT * FROM pokemondata WHERE name="${name}"`
  );
  let JSONData = JSON.stringify(data)
  response.writeHead(200, { 'Content-Type': 'application/json' });
  response.end(JSONData);
}

async function handlerGetComments(request, response) {
  console.log('YOU ENTERED THIS HANDLER  TO GET POKEMON COMMENTS')
  console.log(request.url)
  let id = (request.url).slice(20)
  console.log(id)

  const [data] = await connection.query(
    `SELECT * FROM pokemoncomments WHERE pokemon_id= ${id}`
  );
  let JSONData = JSON.stringify(data)
  response.writeHead(200, { 'Content-Type': 'application/json' });
  response.end(JSONData);
  console.log(data)
}


async function handlePostPokemon(request, response) {
  console.log('YOU ENTERED THIS HANDLER TO POST A NEW POKEMON')

  let payload = "";
  request.on("data", function (chunk) {
    payload = payload.concat(chunk);
  });

  request.on("end", async function () {
    console.log(payload)
    let parsedPayload = JSON.parse(payload)
    console.log('parsed', parsedPayload)
    console.log('name', parsedPayload.name)

    const [data] = await connection.query(
      `INSERT INTO pokemonDATA(id,name,sprite,image,type,generation)
        values(${parsedPayload.id},"${parsedPayload.name}","${parsedPayload.sprite}","${parsedPayload.image}","${parsedPayload.type}",${parsedPayload.generation})`
    );
    let JSONData = JSON.stringify(data)
  });
}

async function handlePostCommentRequest(request, response) {
  console.log('YOU ENTERED THIS HANDLER COMMENT POST')

  let payload = "";
  request.on("data", function (chunk) {
    payload = payload.concat(chunk);
  });

  request.on("end", async function () {

    console.log(payload)
    let parsedPayload = JSON.parse(payload)

    const data = await connection.query(
      `INSERT INTO pokemoncomments(userName,comment,date,pokemon_id)
        values("${parsedPayload.userName}","${parsedPayload.comment}","${parsedPayload.date}","${parsedPayload.pokemonId}")`
    );

    let JSONData = JSON.stringify(data)
  });
  response.writeHead(201, { 'Content-Type': 'application/json' });
  response.end();
}

async function handlerDeleteComment(request, response) {
  console.log('YOU ENTERED THIS HANDLER TO DELETE A COMMENT')
  console.log(request.url)
  let commentId = (request.url).slice(15)
  const [data] = await connection.query(
    `DELETE FROM pokemoncomments WHERE id= ${commentId}`
  );
  let JSONData = JSON.stringify(data)
  response.writeHead(200, { 'Content-Type': 'application/json' });
  response.end(JSONData);
}

async function handlerDeletePokemon(request, response) {
  console.log('YOU ENTERED THIS HANDLER TO DELETE A POKEMON')
  console.log(request.url)
  let pokemonId = (request.url).slice(15);
  console.log('pokemonID', pokemonId)
  const [data] = await connection.query(
    `DELETE FROM pokemondata WHERE id= ${pokemonId}`
  );
  let JSONData = JSON.stringify(data)
  response.writeHead(200, { 'Content-Type': 'application/json' });
  response.end(JSONData);
}



function handleUpdatePokemonPostRequest(request, response) {

  console.log('YOU ENTERED THIS HANDLER TO UPDATE A POKEMON')
  console.log(request.url)

  let payload = "";
  request.on("data", function (chunk) {
    payload = payload.concat(chunk);
  });

  request.on("end", async function () {

    console.log(payload)
    let parsedPayload = JSON.parse(payload)
    console.log(parsedPayload)

     const data = await connection.query(
      `UPDATE pokemondata SET name = "${parsedPayload.name}", sprite="${parsedPayload.sprite}", image = "${parsedPayload.image}", type= "${parsedPayload.type}", generation = ${parsedPayload.generation} WHERE id = ${parsedPayload.id};`
    ); 
  })
  response.writeHead(201, { 'Content-Type': 'application/json' });
  response.end();
}
