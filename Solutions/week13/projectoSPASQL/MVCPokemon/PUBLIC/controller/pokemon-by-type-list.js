import { getPokemonsByType } from "../service/api.js";
import { loader } from "../view/loading-View.js";
import { pokemonByTypeView } from "../view/pokemon-by-type-view.js";
import { errorView } from "../view/error-View.js";


const typesOfPokemonImgs = [{ id: 7, type: "bug", img: "types/bug.png" }, { id: 17, type: "dark", img: "types/dark.png" }, { id: 16, type: "dragon", img: "types/dragon.png" }, { id: 13, type: "electric", img: "types/electric.png" }, { id: 18, type: "fairy", img: "types/fairy.png" }, { id: 2, type: "fighting", img: "types/fighting.png" }, { id: 10, type: "fire", img: "types/fire.png" }, { id: 3, type: "flying", img: "types/flying.png" }, { id: 8, type: "ghost", img: "types/ghost.png" }, { id: 12, type: "grass", img: "types/grass.png" }, { id: 5, type: "ground", img: "types/ground.png" }, { id: 15, type: "ice", img: "types/ice.png" }, { id: 1, type: "normal", img: "types/normal.png" }, { id: 4, type: "poison", img: "types/poison.png" }, { id: 14, type: "psychic", img: "types/psychic.png" }, { id: 6, type: "rock", img: "types/rock.png" }, { id: 9, type: "steel", img: "types/steel.png" }, { id: 11, type: "water", img: "types/water.png" }]

export async function pokemonTypeList(params) {
  console.log('PARAMS',params)
  try {
    loader();
    let pokemonType = typesOfPokemonImgs.find((pokemon)=>pokemon.id==params).type
    let pokemonTypeId=typesOfPokemonImgs.find((pokemon)=>pokemon.id==params).id
    console.log('pokemonTypeId', pokemonTypeId)
    console.log('POKEMONTYPE', pokemonType)
    let data = await getPokemonsByType(pokemonTypeId);   
    console.log('DATA E', data)  
     pokemonByTypeView(data, pokemonType); 
  } catch (err) {
    console.log(err.message)
    errorView();
  }
}


    