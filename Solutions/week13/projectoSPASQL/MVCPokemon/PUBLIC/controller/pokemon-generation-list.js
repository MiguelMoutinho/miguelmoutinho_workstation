import { getPokemonListGeneration } from "../service/api.js";
import { pokemonGenerationView } from "../view/pokemon-generation-view.js";
import { loader } from "../view/loading-View.js";
import { errorView } from "../view/error-View.js";

export async function pokemonGenerationList(generation) {
  try {
    loader();
    let data = await getPokemonListGeneration(generation);
    pokemonGenerationView(data, generation);
  } catch (err) {
    console.log(err.message)
    errorView();
}

}
