import { getSinglePokemonInfo } from "../service/api.js";
import { loader } from "../view/loading-View.js";
import { pokemonProfileView } from "../view/pokemon-profile-view.js";
import { errorView } from "../view/error-View.js";

export async function pokemonProfileList(params) {
  try {
    loader();
    console.log('PARAMETRO ID', params)
    let data = await getSinglePokemonInfo(params)
    pokemonProfileView(data)
  } catch (err) {
    console.log(err.message)
    errorView();
  }

}



