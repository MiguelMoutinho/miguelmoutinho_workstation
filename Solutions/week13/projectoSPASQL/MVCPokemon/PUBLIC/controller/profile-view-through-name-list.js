import { getSinglePokemonInfoByName } from "../service/api.js";
import { loader } from "../view/loading-View.js";
import { pokemonProfileThroughNameView } from "../view/pokemon-profile-view-name.js";
import { errorView } from "../view/error-View.js";

export async function pokemonProfileThroughList(params) {
    console.log('pokemonName', params)
    try {
        loader();
        let data = await getSinglePokemonInfoByName(params);
        pokemonProfileThroughNameView(data)
    } catch (err) {
        console.log(err.message)
        errorView();
    }   
}