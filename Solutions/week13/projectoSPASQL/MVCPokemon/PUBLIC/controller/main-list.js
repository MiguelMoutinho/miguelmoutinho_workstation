import { getPokemonList } from "../service/api.js";
import listView from "../view/main-view.js";
import { loader } from "../view/loading-View.js";

 async function list() {
  try {
  loader();

  let data = await getPokemonList();
  listView(data) 
} catch (err) {
  console.log(err.message)
  errorView();
}
 }

export default list;
