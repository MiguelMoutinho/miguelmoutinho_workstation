import characterListController from "./controller/main-list.js";
import {pokemonProfileList} from "./controller/profile-view-list.js" 
import {pokemonTypeList} from "./controller/pokemon-by-type-list.js" 
import {pokemonGenerationList} from "./controller/pokemon-generation-list.js" 
import {pokemonProfileThroughList} from "./controller/profile-view-through-name-list.js"
import {addPokemonList} from "./controller/add-pokemon-list.js"
 import {updatePokemonList} from "./controller/update-pokemon-list.js" 

  
const ROUTES = {
  LIST: "/",
};

const routes = [{ path: /^\/?$/, init: characterListController }, {path: /^\/pokemonProfile\/(?<id>\d+)\/?$/gi, init: pokemonProfileList },  {path: /^\/pokemonType\/(?<id>\d+)\/?$/gi, init: pokemonTypeList},
{path: /^\/generation\/.+/, init: pokemonGenerationList}, {path: /^\/pokemon\/.+/, init: pokemonProfileThroughList}, {path: /^\/addPokemon/, init: addPokemonList},{path: /^\/updatePokemon/, init: updatePokemonList}];

 function getRoute(path) {
  return routes.find(function (route) { 
    return path.match(route.path);
  });
} 

export { ROUTES, getRoute };
