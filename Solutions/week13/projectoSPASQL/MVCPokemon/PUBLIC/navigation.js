import { ROUTES, getRoute } from "./routes.js";

export function goTo(path,dataContent) {
  console.log('NAVIGATION', dataContent)
  history.pushState(null, null, path)
  const route = getRoute(path);
  if (!route) {
    goTo(ROUTES.LIST);
    return;
  }
  let params;

  if (path === "/") {
    params = ""
  }

  else if (/^\/pokemon\/.+/.test(path)) {
    params = path.match(/^\/pokemon\/.+/)[0].slice(9)
    console.log(params)
  }

  else if (/^\/generation\/.+/.test(path)) {
    params = path.match(/^\/generation\/.+/)[0].slice(12)
  }

  else if (path==="/addPokemon") {
    console.log('ENTREI AQUI')
   params=""
  }

  else if (path==="/updatePokemon") {
    console.log('ENTREI AQUI')
   params = dataContent
  }

  else {
    params = path.match(/(\d)+/).splice(0)[0];
  }
  route.init(params);
}

export function goTo2(path) {
  const route = getRoute(path);
  if (!route) {
    goTo(ROUTES.LIST);
    return;
  }

  let params;

  if (path === "/") {
    params = ""
  }

  else if (/^\/pokemon\/.+/.test(path)) {
    params = path.match(/^\/pokemon\/.+/)[0].slice(9)
    console.log(params)
  }

  else if (path==="/addPokemon") {
    params=""
   }

  else {
    params = path.match(/(\d)+/).splice(0)[0];
    console.log(params)
  }

  route.init(params);
}

function cb(e) {
  let pathToGo = e.path[0].location.href;
  let updatedPath = pathToGo.slice(21)
  goTo2(updatedPath)
}

window.addEventListener('popstate', cb) 