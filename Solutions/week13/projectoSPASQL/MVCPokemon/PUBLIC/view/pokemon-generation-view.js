import { createNavbar } from "./utilities/navbar-Creator.js";
import { mainContentCreator } from "./Utilities/mainContent-creator.js"
import { sideCardProfileByType } from "./utilities/side-card-component-creator.js"
import { createPokemonSpriteDiv } from "./Utilities/create-pokemon-sprite-div.js"
const root = document.querySelector("#root");




export function pokemonGenerationView(pokemon, generation) {

     root.innerHTML = ""
    createNavbar(); 
    mainContentCreator(generation);
    pokemon.map(createPokemonSpriteDiv);

    const listeners = document.querySelectorAll('.boxSize')
    listeners.forEach((listener) => {
        listener.addEventListener('click', () => sideCardProfileByType(pokemon, (listener.id)))
    })
}