import { createNavbar } from "./utilities/navbar-Creator.js";
import { mainContentCreatorLoading } from "./Utilities/mainContent-creator-loading.js"
const root = document.querySelector("#root");

export function loader() {
    root.innerHTML = ""
    createNavbar();
    mainContentCreatorLoading();
}