import { createNavbar } from "./utilities/navbar-Creator.js";
import { mainContentError } from "./utilities/mainContent-error.js"
const root = document.querySelector("#root");

export function errorView() {
    root.innerHTML=""
    createNavbar();
    mainContentError();
}