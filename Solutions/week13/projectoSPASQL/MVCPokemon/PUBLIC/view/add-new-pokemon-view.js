const root = document.querySelector('#root');

import { createNavbar } from './utilities/navbar-Creator.js';

import { createForm } from './utilities/create-form.js';

export function addNewPokemonView() {
    root.innerHTML=''
    createNavbar();
    createForm();
}

