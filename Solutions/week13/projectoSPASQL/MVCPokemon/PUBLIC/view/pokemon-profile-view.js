import { createNavbar } from "./utilities/navbar-Creator.js";

import { createPokemonProfileView } from "./utilities/pokemon-profile-view.js";

export function pokemonProfileView(data) {
    const rootDiv = document.querySelector('#root')
    rootDiv.innerHTML = ""
    createNavbar();
    createPokemonProfileView(data)
}


