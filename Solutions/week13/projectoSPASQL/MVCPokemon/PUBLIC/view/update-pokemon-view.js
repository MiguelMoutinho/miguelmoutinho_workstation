const root = document.querySelector('#root');

import { createNavbar } from './utilities/navbar-Creator.js';

import { createFormToUpdate } from './utilities/create-form.js';

export function updatePokemonView(params) {
    root.innerHTML=''
    createNavbar();
    createFormToUpdate(params);
}

