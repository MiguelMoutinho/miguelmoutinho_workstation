import { goTo } from "../../navigation.js"

const typesOfPokemonImgs = [{ type: "bug", img: "view/utilities/types/bug.png" }, { type: "dark", img: "view/utilities/types/dark.png" }, { type: "dragon", img: "view/utilities/types/dragon.png" }, { type: "electric", img: "view/utilities/types/electric.png" }, { type: "fairy", img: "view/utilities/types/fairy.png" }, { type: "fighting", img: "view/utilities/types/fighting.png" }, { type: "fire", img: "view/utilities/types/fire.png" },
{ type: "flying", img: "view/utilities/types/flying.png" }, { type: "ghost", img: "view/utilities/types/ghost.png" }, { type: "grass", img: "view/utilities/types/grass.png" },
{ type: "ground", img: "view/utilities/types/ground.png" }, { type: "ice", img: "view/utilities/types/ice.png" }, { type: "normal", img: "view/utilities/types/normal.png" },
{ type: "poison", img: "view/utilities/types/poison.png" }, { type: "psychic", img: "view/utilities/types/psychic.png" }, { type: "rock", img: "view/utilities/types/rock.png" },
{ type: "steel", img: "view/utilities/types/steel.png" }, { type: "water", img: "view/utilities/types/water.png" }]

export function sideCardProfileByType(pokemon, selectedId) {

  console.log('O QUE E ESTE POKEMON', pokemon)

  let cardInfo = document.querySelector('.pokemonInfo')
  cardInfo.innerHTML = ""
  let cardDiv = document.createElement("div");
  cardDiv.classList.add('pokemonCardInfo')
  cardInfo.appendChild(cardDiv)
  const titleDiv = document.createElement("div")
  titleDiv.classList.add('title')
  cardDiv.appendChild(titleDiv)
  const nameAndIdDiv = document.createElement("div");
  nameAndIdDiv.classList.add('name')
  nameAndIdDiv.innerHTML = `<h2>${pokemon.find((pokemon) => pokemon.id == selectedId).name} #${pokemon.find((pokemon) => pokemon.id == selectedId).id}</h2>`
  titleDiv.appendChild(nameAndIdDiv)
  const typesDiv = document.createElement("div")
  typesDiv.classList.add('typesDiv')
  let pokemonType = pokemon.find((pokemon) => pokemon.id == selectedId).type
  let arrayOfTypes = pokemonType.split(", ")
  console.log(arrayOfTypes)
  for (let j = 0; j < arrayOfTypes.length; j++) {
    let selectedLogoType = typesOfPokemonImgs.find((type) => type.type == arrayOfTypes[j]).img
    typesDiv.innerHTML += `<img class='logoSideCard' src=${selectedLogoType}>`
  };
  titleDiv.appendChild(typesDiv)
  const imageDiv = document.createElement("div")
  imageDiv.classList.add('imageSideCard')
  imageDiv.innerHTML = `<img class='pokemonImageCard' src="${pokemon.find((pokemon) => pokemon.id == selectedId).image}">`
  cardDiv.appendChild(imageDiv)
  const editAndDeleteDiv = document.createElement("div");
  editAndDeleteDiv.classList.add('editAndDeleteDivStyle');
  const editBtn = document.createElement("button");
  editBtn.innerHTML='EDIT'
  const deleteBtn = document.createElement("button");
  deleteBtn.innerHTML='DELETE'
  editAndDeleteDiv.appendChild(editBtn);
  editAndDeleteDiv.appendChild(deleteBtn);
  cardDiv.appendChild(editAndDeleteDiv);
  const insideDiv = document.createElement("div");
  insideDiv.classList.add('inside');
  cardDiv.appendChild(insideDiv)
  const findMoreDiv = document.createElement("div");
  findMoreDiv.classList.add('findMoreStyle');
  insideDiv.appendChild(findMoreDiv)
  findMoreDiv.innerHTML = "<h3>Find out more!</h3><img class='pokeballMoreInfo' src='view/utilities/pokeball.png'>"
  let findMoreButton = document.querySelector('.pokeballMoreInfo');
  findMoreButton.addEventListener('click', () => goTo(`/pokemonProfile/${selectedId}`))


  async function deletePokemon(){
    console.log('this pokemon ID is', selectedId)
    console.log('deletepokemoninfunction')
    const response = await fetch(`http://localhost:8000/deletePokemon/${selectedId}`);
    console.log('pokemon deleted') 
    goTo("/")
  }

  async function updatePokemon(){
    let pokemonInfo = pokemon.find((el)=>el.id == selectedId)
    console.log(pokemonInfo)
    console.log('this pokemon ID is', selectedId)
    console.log('carreguei no EDIT')
     goTo("/updatePokemon", pokemonInfo) 
  
   }
  deleteBtn.addEventListener('click',deletePokemon)

   editBtn.addEventListener('click', updatePokemon) 
}
