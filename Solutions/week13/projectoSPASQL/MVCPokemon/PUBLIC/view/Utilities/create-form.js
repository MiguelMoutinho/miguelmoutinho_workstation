import { goTo } from "../../navigation.js"

export function createForm() {
    const rootDiv = document.querySelector('#root')
    const main = document.createElement("div");
    main.classList.add('mainForm')
    rootDiv.appendChild(main)
    const formDiv = document.createElement("div");
    formDiv.classList.add('formStyle')
    const title = document.createElement("h1")
    title.textContent="Add a new Pokemon!"
    formDiv.innerHTML="<label for='id'>id:</label><input name='id' type='number'/><label for='name'>name:</label><input name='name' type='text'/><label for='sprite'>sprite:</label><input name='sprite' type='text'/><label for='image'>image:</label><input name='image' type='text'/><label for='type'>type:</label><input name='type' type='text'/><label for='generation'>generation:</label><input name='generation' type='number'/> <input class='btnSubmit' type='submit' value='Submit' />"
    main.appendChild(title)
    main.appendChild(formDiv)

    const submitButton = document.querySelector('.btnSubmit')
    submitButton.addEventListener('click',cb)
    function cb(event) {
        event.preventDefault(); 
        let id = document.querySelector('input[name="id"]').value
        let name = document.querySelector('input[name="name"]').value
        let sprite = document.querySelector('input[name="sprite"]').value
        let image = document.querySelector('input[name="image"]').value
        let type = document.querySelector('input[name="type"]').value
        let generation = document.querySelector('input[name="generation"]').value
        console.log('id',id)
        console.log('name',name)
        console.log('sprite',sprite)
        console.log('image',image)
        console.log('type', type)
        console.log('generation',generation)

        let content = { id: id, name:name, sprite:sprite,image:image, type:type, generation:generation }
        console.log(content)
        console.log('SUBMIT DADOS')
        sendDataToServer(content,()=>console.log('data sent successf'))
        goTo("/")
    }


    function sendDataToServer(data, onSuccess, onError) {

        fetch('http://localhost:8000/form', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(onSuccess)
            .catch(onError);
    }
    
}

export function createFormToUpdate(params) {
    console.log('OS DADOS CHEGARAM AO FORM PA UPDATE', params)
    const rootDiv = document.querySelector('#root')
    const main = document.createElement("div");
    main.classList.add('mainForm')
    rootDiv.appendChild(main)
    const formDiv = document.createElement("div");
    formDiv.classList.add('formStyle')
    const title = document.createElement("h1")
    title.textContent="Update this Pokemon!"
    formDiv.innerHTML="<label for='name'>name:</label><input name='name' type='text'/><label for='sprite'>sprite:</label><input name='sprite' type='text'/><label for='image'>image:</label><input name='image' type='text'/><label for='type'>type:</label><input name='type' type='text'/><label for='generation'>generation:</label><input name='generation' type='number'/> <input class='btnSubmit' type='submit' value='Submit' />"
    main.appendChild(title)
    main.appendChild(formDiv)

    const submitButton = document.querySelector('.btnSubmit')
    submitButton.addEventListener('click',cb)
    function cb(event) {
        event.preventDefault();
        let name = document.querySelector('input[name="name"]').value
        let sprite = document.querySelector('input[name="sprite"]').value
        let image = document.querySelector('input[name="image"]').value
        let type = document.querySelector('input[name="type"]').value
        let generation = document.querySelector('input[name="generation"]').value
        let formDataArray=[0,name,sprite,image,type,generation]
        console.log(formDataArray)
        let dataAlreadyInPokemon = Object.values(params)
        console.log(dataAlreadyInPokemon)
        let finalDataToSend = [];
        finalDataToSend.push(dataAlreadyInPokemon[0]);
        for (let i=1;i<formDataArray.length;i++){
            if(formDataArray[i]){
                finalDataToSend.push(formDataArray[i]) 
            }
            else {
             finalDataToSend.push(dataAlreadyInPokemon[i]) 
        }
    }

        console.log('FINALDATA', finalDataToSend)

        let content = {id:finalDataToSend[0], name:finalDataToSend[1], sprite:finalDataToSend[2],image:finalDataToSend[3], type:finalDataToSend[4], generation:finalDataToSend[5] }
        console.log(content)
        console.log('UPDATE DADOS')
         sendDataToServerToUpdate(content,()=>console.log('data sent successf')) 
         goTo("/")
    }


    function sendDataToServerToUpdate(data, onSuccess, onError) {

        fetch(`http://localhost:8000/updatePokemon/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(onSuccess)
            .catch(onError);
    }
    
}




