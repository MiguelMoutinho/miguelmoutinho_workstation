export function createPokemonProfileView(data) {
    console.log('DATAAAAA-ID', data.id)
    let pokemonId = data.id;
    const rootDiv = document.querySelector('#root')
    const container = document.createElement("div");
    container.classList.add('containerProfilePage')
    rootDiv.appendChild(container)
    const title = document.createElement("h1")
    title.classList.add('titleProfilePage')
    const divMidSection = document.createElement("div");
    divMidSection.classList.add('divMidSectionStyle')
    const imageSlot = document.createElement("div");
    imageSlot.classList.add('imageSlotProfilePage')
    const formCommentSection = document.createElement("div");
    formCommentSection.classList.add('formCommentStyle')
    formCommentSection.innerHTML = "<label for='name'>userName:</label><input name='userName' type='text'/><label for='name'>comment:</label><input name='comment' type='text'/><input class='btnSubmit' type='submit' value='Submit' />"
    const bottom = document.createElement("div")
    bottom.classList.add('bottomContainerStyle')
    bottom.innerHTML = "<table class= 'commentsStyle'> <tr><th>User</th><th>Comment</th><th>Time</th></tr></table>"
    container.appendChild(title)
    container.appendChild(divMidSection)
    divMidSection.appendChild(imageSlot)
    divMidSection.appendChild(formCommentSection)
    container.appendChild(bottom)
    title.innerHTML = `${data.name}`
    imageSlot.innerHTML = `<img class='resize' src="${data.image}">`

    const btnSubmitComments = document.querySelector('.btnSubmit')
    btnSubmitComments.addEventListener('click', cb)
    updateCommentSection(pokemonId);

    function cb(event) {
        console.log('entrei neste evento pa submeter')
        event.preventDefault();
        let userName = document.querySelector('input[name="userName"]').value
        let comment = document.querySelector('input[name="comment"]').value
        let date = new Date().toISOString().substring(11, 19)
        let pokemonId = data.id
        let content = { userName: userName, comment: comment, date: date, pokemonId: pokemonId }
        sendDataToServer(content)
        /*   sendDataToServer(content, updateCommentSection(pokemonId)) */
    }

    async function updateCommentSection(pokemonId) {
        console.log('UPDATE A TABLE!!!!')
        const response = await fetch(`http://localhost:8000/getPokemonComments/${pokemonId}`)
        const data = await response.json();
        console.log('DATA PARA UPDATE COMMENTS', data)
        bottom.innerHTML = ""
        bottom.innerHTML = "<table class= 'commentsStyle'> <tr><th>User</th><th>Comment</th><th>Time</th></tr></table>"
        for (let i = 0; i < data.length; i++) {
            let usersTable = document.querySelector('.commentsStyle')
            let row = usersTable.insertRow(-1);
            let user = data[i].userName;
            let comment = data[i].comment;
            let date = data[i].date;
            let commentID = data[i].id
            /*             row.innerHTML = ` <td>${user}</td> <td>${comment}</td> <td>${date}</td><td id= ${commentID} class='testDelete'><b>DELETE</b></td>`   */
            row.innerHTML = ` <td>${user}</td> <td>${comment}</td> <td>${date}</td><button id= ${commentID} class='testDelete'>DELETE</button></td>`
        }

        const tableEl = document.querySelector('.commentsStyle')

        tableEl.addEventListener('click', onDeleteRow)

        function onDeleteRow(event) {
            if (event.target.classList.contains('testDelete')) {
                console.log('CARREGASTE NO BOTAO')
                const btn= event.target;
                console.log('COMMENT TO REMOVE-', event.target.id)
                btn.closest("tr").remove();
                let commentId = event.target.id
                 fetch(`http://localhost:8000/deleteComment/${commentId}`) 
            } else {
                return
            }
    }

}

    async function sendDataToServer(data, onSuccess, onError) {

        const response = await fetch('http://localhost:8000/formComments', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })

        const response2 = await fetch(`http://localhost:8000/getPokemonComments/${pokemonId}`)
        const data2 = await response2.json();

        console.log('DATA PARA UPDATE COMMENTS', data2)
        bottom.innerHTML = ""
        bottom.innerHTML = "<table class= 'commentsStyle'> <tr><th>User</th><th>Comment</th><th>Time</th></tr></table>"
        for (let i = 0; i < data2.length; i++) {
            let usersTable = document.querySelector('.commentsStyle')
            let row = usersTable.insertRow(-1);
            let user = data2[i].userName;
            let comment = data2[i].comment;
            let date = data2[i].date;
            let commentID = data2[i].id
            /*             row.innerHTML = ` <td>${user}</td> <td>${comment}</td> <td>${date}</td><td id= ${commentID} class='testDelete'><b>DELETE</b></td>`   */
            row.innerHTML = ` <td>${user}</td> <td>${comment}</td> <td>${date}</td><button id= ${commentID} class='testDelete'>DELETE</button></td>`

        }
        const tableEl = document.querySelector('.commentsStyle')

        function onDeleteRow(event) {
            if (event.target.classList.contains('testDelete')) {
                console.log('CARREGASTE NO BOTAO')
                const btn= event.target;
                btn.closest("tr").remove();
                let commentId = event.target.id
                fetch(`http://localhost:8000/deleteComment/${commentId}`) 
            } else {
                return
            }

        }
        tableEl.addEventListener('click', onDeleteRow)
        /*         tableEl.addEventListener('click', ()=>console.log('clickedOnTable!'))
         */
    }
    /*   async function deleteComment(id) {
         console.log('CLICK EFECTUADO')
         console.log(id)
         const response = await fetch(`http://localhost:8000/deleteComment/${id}`)
     } */




}






