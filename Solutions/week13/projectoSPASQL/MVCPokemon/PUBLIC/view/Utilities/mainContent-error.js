const root = document.querySelector("#root");

export function mainContentError() {

    const rootDiv = document.querySelector('#root')
    const main = document.createElement("div");
    main.classList.add('main')
    rootDiv.appendChild(main)
    const divAllPokemonSpace = document.createElement("div");
    const pokemonProfileSpace = document.createElement("div");
    divAllPokemonSpace.classList.add(`errorImage`);
    pokemonProfileSpace.classList.add(`pokemonInfo`)
    const title = document.createElement("div");
    title.classList.add(`titleGeneration`);
    const container = document.createElement("div");
    container.classList.add('containerStyle')
    main.appendChild(container);
    container.appendChild(title)
    container.appendChild(divAllPokemonSpace)
    main.appendChild(pokemonProfileSpace);
    title.innerHTML = "<h1>Page not Found !</h1>"
    divAllPokemonSpace.innerHTML = "<img style='height: 70%; width:70%; object-fit: contain' src='view/utilities/teamRocket.png'>"

}