import {goTo} from "../../navigation.js"


const typesOfPokemonImgs = [{ id: 7, type: "bug", img: "types/bug.png" }, { id:17, type: "dark", img: "types/dark.png" }, {id:16, type: "dragon", img: "types/dragon.png" }, {id:13 , type: "electric", img: "types/electric.png" }, {id:18, type: "fairy", img: "types/fairy.png" }, {id:2, type: "fighting", img: "types/fighting.png" }, {id:10 ,type: "fire", img: "types/fire.png" }, {id:3, type: "flying", img: "types/flying.png" }, {id:8, type: "ghost", img: "types/ghost.png" }, {id:12, type: "grass", img: "types/grass.png" }, {id:5, type: "ground", img: "types/ground.png" }, {id:15, type: "ice", img: "types/ice.png" }, {id:1, type: "normal", img: "types/normal.png" }, {id:4, type: "poison", img: "types/poison.png" }, {id:14 , type: "psychic", img: "types/psychic.png" }, {id:6, type: "rock", img: "types/rock.png" }, {id:9 , type: "steel", img: "types/steel.png" }, {id:11, type: "water", img: "types/water.png" }]

export function createNavbar() {

    const rootDiv = document.querySelector('#root')
    const navbar = document.createElement("div");
    navbar.classList.add('pokemonNavbar');
    rootDiv.appendChild(navbar)
    const pokemonLogoImg = document.createElement("div");
    pokemonLogoImg.classList.add('imageSize')
    pokemonLogoImg.innerHTML = "<img class='pokemonLogo' src='view/utilities/logo.png'></div>";
    navbar.appendChild(pokemonLogoImg)
    const typeContainer = document.createElement("div");
    typeContainer.classList.add('typesContainer')
    navbar.appendChild(typeContainer)
    const selectGenerationDiv = document.createElement("div")
    selectGenerationDiv.classList.add('selectGenerationStyle')
    selectGenerationDiv.innerHTML="<div class='custom-select'><select><option value='0'>Select Pokemon Generation</option><option value='1'>Generation 1</option><option value='2'>Generation 2</option><option value='3'>Generation 3</option></select></div>"
    navbar.appendChild(selectGenerationDiv)
    const pokemonSearchDiv = document.createElement("div");
    pokemonSearchDiv.classList.add('SearchDiv')
    pokemonSearchDiv.innerHTML = "<input></input><br><img class='searchButton'src='view/utilities/pokeball.png'>"
    navbar.appendChild(pokemonSearchDiv)
  
      for (let i = 0; i < typesOfPokemonImgs.length; i++) {
  
        let pokemonType = typesOfPokemonImgs[i].id
        typeContainer.innerHTML = typeContainer.innerHTML + `<img class='pokemonTypeLogo' id = ${pokemonType} src="view/utilities/${typesOfPokemonImgs[i].img}">`
    }  

    const logoTypes = document.querySelectorAll('.pokemonTypeLogo')
  
   logoTypes.forEach((listener) => {
    listener.addEventListener('click', ()=> goTo(`/pokemonType/${listener.id}`))
}); 

    const selector = document.querySelector('.selectGenerationStyle')
    selector.addEventListener('change', goToSelectedGeneration)

    function goToSelectedGeneration(event) {
      let generationNumber = (event.target.value)
      if(generationNumber === 1) {

        return goTo("/")
      }
      goTo(`/generation/${generationNumber}`)
      
    }

    const searchButton = document.querySelector('.searchButton');
    searchButton.addEventListener('click', ()=>{
      let searchInput = document.querySelector('input').value
      console.log(searchInput)
       goTo(`/pokemon/${searchInput}`) 
    })

    const createNewPokemonButton = document.querySelector('.pokemonLogo')
    createNewPokemonButton.addEventListener('click', ()=>goTo('/addPokemon'))
  }
