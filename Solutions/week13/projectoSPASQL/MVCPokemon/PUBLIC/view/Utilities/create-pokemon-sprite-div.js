export function createPokemonSpriteDiv (pokemon) {
    const newDiv = document.createElement("div");
    newDiv.classList.add('boxSize')
    newDiv.innerHTML = `<img src="${pokemon.sprite}">`
    newDiv.id = pokemon.id
    let container = document.querySelector('.pokemonPlacers')
    container.appendChild(newDiv)
  }