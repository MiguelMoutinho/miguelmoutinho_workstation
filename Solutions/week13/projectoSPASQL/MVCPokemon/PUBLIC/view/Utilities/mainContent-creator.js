

export function mainContentCreator(generation) {
    const rootDiv = document.querySelector('#root')
    const main = document.createElement("div");
    main.classList.add('main')
    rootDiv.appendChild(main)
    const divAllPokemonSpace = document.createElement("div");
    const pokemonProfileSpace = document.createElement("div");
    divAllPokemonSpace.classList.add(`pokemonPlacers`);
    pokemonProfileSpace.classList.add(`pokemonInfo`)
    const generationTitle = document.createElement("div");
    generationTitle.classList.add(`titleGeneration`);
    const container = document.createElement("div");
    container.classList.add('containerStyle')
    main.appendChild(container);
    container.appendChild(generationTitle)
    container.appendChild(divAllPokemonSpace)
    main.appendChild(pokemonProfileSpace);
    generationTitle.innerHTML = `<h1>Generation ${generation}</h1>`
}


export function mainContentCreatorByType(type) {
    const rootDiv = document.querySelector('#root')
    const main = document.createElement("div");
    main.classList.add('main')
    rootDiv.appendChild(main)
    const divAllPokemonSpace = document.createElement("div");
    const pokemonProfileSpace = document.createElement("div");
    divAllPokemonSpace.classList.add(`pokemonPlacers`);
    pokemonProfileSpace.classList.add(`pokemonInfo`)
    const generationTitle = document.createElement("div");
    generationTitle.classList.add(`titleGeneration`);
    const container = document.createElement("div");
    container.classList.add('containerStyle')
    main.appendChild(container);
    container.appendChild(generationTitle)
    container.appendChild(divAllPokemonSpace)
    main.appendChild(pokemonProfileSpace);
    generationTitle.innerHTML = `<h1>type ${type}</h1>`
}