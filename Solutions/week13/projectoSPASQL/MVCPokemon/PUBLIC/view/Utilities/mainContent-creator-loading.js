const root = document.querySelector("#root");

export function mainContentCreatorLoading() {
    const rootDiv = document.querySelector('#root')
    const main = document.createElement("div");
    main.classList.add('main')
    rootDiv.appendChild(main)
    const divAllPokemonSpace = document.createElement("div");
    const pokemonProfileSpace = document.createElement("div");
    divAllPokemonSpace.classList.add(`loadingGifContainer`);
    pokemonProfileSpace.classList.add(`pokemonInfo`)
    const title = document.createElement("div");
    title.classList.add(`titleGeneration`);
    const container = document.createElement("div");
    container.classList.add('containerStyle')
    main.appendChild(container);
    container.appendChild(title)
    container.appendChild(divAllPokemonSpace)
    main.appendChild(pokemonProfileSpace);
    title.innerHTML = "<h1>Loading...</h1>"
    divAllPokemonSpace.innerHTML = "<img src='view/utilities/loading.gif'>"

}