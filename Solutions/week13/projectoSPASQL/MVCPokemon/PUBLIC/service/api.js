export { getPokemonList, getPokemonsByType, getSinglePokemonInfo, getPokemonListGeneration, getSinglePokemonInfoByName };

const POKEMONENDPOINT = "https://pokeapi.co/api/v2/"


async function getPokemonList() {

  const response = await fetch('http://localhost:8000/getGeneration1Pokemon')
  const data = await response.json();
  console.log(data)
  return data
}

async function getPokemonListGeneration(generation) {

  const response = await fetch(`http://localhost:8000/getGeneration${generation}Pokemon`)
  const data = await response.json();
  return data;
}

async function getPokemonsByType(params) {
  const response = await fetch(`http://localhost:8000/getPokemonByType/${params}`)
    const data = await response.json();
    return data;
}

async function getSinglePokemonInfo(params) {
  const response = await fetch(`http://localhost:8000/getPokemonInfoByID/${params}`)
  const [data] = await response.json();
  return data;
}

async function getSinglePokemonInfoByName(params) {
  const response = await fetch(`http://localhost:8000/getPokemonInfoByName/${params}`)
  const [data] = await response.json();
  return data;

}

