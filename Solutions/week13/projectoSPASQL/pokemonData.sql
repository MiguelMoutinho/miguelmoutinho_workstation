DROP DATABASE pokemon;
 
 CREATE DATABASE pokemon;

  USE pokemon;


CREATE TABLE pokemonDATA (
	id INT NOT NULL PRIMARY KEY,
	name VARCHAR(25) NOT NULL,
	sprite VARCHAR(500) NOT NULL,
	image VARCHAR(500) NOT NULL,
	type VARCHAR(25) NOT NULL,
	generation INT NOT NULL
);


CREATE TABLE pokemonComments (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	userName VARCHAR(25) NOT NULL,
	comment VARCHAR(500) NOT NULL,
	date VARCHAR(50) NOT NULL,
	pokemon_id INT NOT NULL,
	FOREIGN KEY (pokemon_id) REFERENCES pokemonDATA(id)
	ON DELETE CASCADE
);

INSERT INTO pokemonDATA(id,name,sprite,image,type,generation)
values(4,'charmander','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/4.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/4.svg','fire',1),
(9,'blastoise','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/9.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/9.svg','water',1),
(10,'caterpie','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/10.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/10.svg','bug',1),
(18,'pidgeot ','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/18.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/18.svg','flying',1),
(25,'pikachu','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/25.svg','electric',1),
(35,'clefairy ','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/35.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/35.svg','fairy',1),
(37,'vulpix','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/37.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/37.svg','fire',1),
(64,'kadabra','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/64.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/64.svg','psychic',1),
(66,'machop','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/66.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/66.svg','fighting',1),
(107,'hitmonchan','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/107.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/107.svg','fighting',1),
(92,'gastly','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/92.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/92.svg','ghost',1),
(120,'staryu','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/120.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/120.svg','water',1),
(133,'eevee','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/133.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/133.svg','normal',1),
(148,'dragonair','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/148.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/148.svg','dragon',1),
(152,'chikorita','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/152.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/152.svg','grass',2),
(254,'sceptile','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/254.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/254.svg','grass',3),
(204,'pineco','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/204.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/204.svg','bug',2),
(158,'totodile','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/158.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/158.svg','water',3),
(362,'glalie','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/362.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/362.svg','ice',3),
(87,'dewgong','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/87.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/87.svg','ice',1),
(74,'geodude','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/74.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/74.svg','rock',1),
(377,'regirock','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/377.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/377.svg','rock',3),
(24,'arbok','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/24.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/24.svg','poison',1),
(317,'swalot','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/317.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/317.svg','poison',3),
(241,'miltank','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/214.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/214.svg','normal',2),
(104,'cubone','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/104.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/104.svg','ground',1),
(383,'groudon','https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/383.png', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/383.svg','ground',3);








