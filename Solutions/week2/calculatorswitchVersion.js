function calculator(number1, number2, symbol) {

    var input1 = number1;

    var input2 = number2;

    if (typeof (input1) !== 'number' || typeof (input2) !== 'number') {
        return ('you have to insert proper numbers!')
    }

    switch (symbol) {
        case '+':
            var sum = number1 + number2;
            return sum;

        case '-':
            var difference = number1 - number2;
            return difference;

        case '*':
            var product = number1 * number2;
            return product;

        case '/':
            var quotient = number1 / number2;
            return quotient;

        default:
            return ('Please introduce a valid operator, you have 4 to choose from : +,-,* and /')
    }

}

