function operation(number1, number2, symbol) {

    var input1 = number1;

    var input2 = number2;

    if(typeof(input1) !== 'number' || typeof(input2) !== 'number') {
        console.log('you have to insert proper numbers!')
        return null
    }
    if (symbol === '+') {
        var sum = number1 + number2;
        return sum;
    }
    else if (symbol === '-') {
        var difference = number1 - number2;
        return difference;
    }
    else if (symbol === '*') {
        var product = number1 * number2;
        return product;
    }
    else if (symbol === '/') {
        var quotient = number1 / number2;
        return quotient;
    }
    else {
        console.log('Please introduce a valid operator, you have 4 to choose from : +,-,* and /')
        return null;
    }

}

console.log(operation(7,6,''));

