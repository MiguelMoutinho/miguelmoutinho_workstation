
function smallestNumber(listOfNumbers) {
    var smallest = listOfNumbers[0];

    for (var i = 1; i < listOfNumbers.length; i++)
        if (listOfNumbers[i] < smallest) {
            smallest = listOfNumbers[i];
        }
    return (smallest);
}