
function countVowels(sentence) {
    var vowels = ['a', 'e', 'i', 'o', 'u'];

    var sentence;

    var count = 0;

    for (var i = 0; i < sentence.length; i++) {
        for (var j = 0; j < vowels.length; j++) {
            if (sentence[i] === vowels[j]) {
                count++
            }
        }
    }
    return (count);
}

