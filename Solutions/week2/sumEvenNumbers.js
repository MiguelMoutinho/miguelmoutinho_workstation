
function sumEvenNumbers(listOfNumbers) {
    var sum = 0
    for (var i = 0; i < listOfNumbers.length; i++) {
        if (listOfNumbers[i] % 2 == 0) {
            sum = sum + listOfNumbers[i]
        }
    }
    return sum;
}