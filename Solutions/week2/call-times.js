
function callTimes(times) {
    return function (string) {
        for (var i = 0; i < times; i++) {
            console.log(string);
        }
    }
}

var executeFunction = callTimes(7);

console.log(executeFunction('You just executed this function!!!'))
