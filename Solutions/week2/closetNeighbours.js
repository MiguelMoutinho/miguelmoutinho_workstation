
function closetNeighbours(listOfNumbers) {
    var pairOfNumbers = [];

    var smallest;

    for (var i = 0; i < listOfNumbers.length; i++) {

        var difference = Math.abs(listOfNumbers[i] - listOfNumbers[i + 1]);

        if (!smallest || difference < smallest) {


            smallest = difference;

            pairOfNumbers[0] = listOfNumbers[i]
            pairOfNumbers[1] = listOfNumbers[i + 1]


        }
    }

    return (pairOfNumbers)
}
