function transformToPigLatin(string) {
    var separatedWords = []
    for (var i = 0; i < string.length; i++) {
        if (string[i] === ' ') {
            separatedWords.push(string.slice(0, i));
            string = string.slice(i + 1, string.length);
            i = 0
        }
       
    }
    separatedWords.push(string)

    var newList = [];

    var vowels = ['a', 'e', 'i', 'o', 'u']

    for (var j = 0; j < separatedWords.length; j++) {

        for (var k = 0; k < vowels.length; k++) {
            if (separatedWords[j][0] === vowels[k]) {
                newList.push(separatedWords[j] + 'way')
                break;
            }

            if (separatedWords[j][0] !== vowels[k] && separatedWords[j][1] === vowels[k]) {
                var firstChar = separatedWords[j][0];

                newList.push(separatedWords[j].slice(1) + firstChar + 'ay')
                break;
            }

            else {
                var firstChar = separatedWords[j][0];
                var secondChar = separatedWords[j][1]
                newList.push(separatedWords[j].slice(2) + firstChar + secondChar + 'ay')
                break
            }

        }
    }

    var newStringPigLatin = ''

    for(var l=0; l<newList.length;l++) {
        newStringPigLatin = newStringPigLatin + newList[l] + ' ' 
    }
    return (newStringPigLatin);
}



