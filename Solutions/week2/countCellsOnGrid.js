var grid = [[0, 0, 0, 1], [0, 0, 1, 0], [0, 1, 0, 1], [0, 0, 1, 0]];


function countPaintedCellsOnGrid(grid) {


    var counter = 0

    for (var i = 0; i < grid.length; i++) {
        var row = grid[i]

        for (var j = 0; j < row.length; j++) {
            if (row[j] === 1) {
                counter++
            }
        }
    }

    return (counter)

}


