
function censorSentence(string, forbiddenWords) {

    var separatedWords = string.split(' ', string.length);

    var filteredString = [];

    for (var i = 0; i < separatedWords.length; i++) {
        for (var j = 0; j < forbiddenWords.length; j++) {
            if ((separatedWords[i] !== forbiddenWords[j])) {
                if (!(filteredString.includes(separatedWords[i]))) {
                    filteredString.push(separatedWords[i])
                }
            }

        }
    }

    for (var k = 0; k < filteredString.length; k++) {
        for (var l = 0; l < forbiddenWords.length; l++) {
            if (filteredString[k] === forbiddenWords[l]) {
                filteredString.splice(k, 1)
            }

        }
    }

    var convertedFilteredString = filteredString.join(' ');

    return (convertedFilteredString);

}