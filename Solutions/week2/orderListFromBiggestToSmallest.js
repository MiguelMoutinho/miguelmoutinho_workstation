
function orderList(listOfNumbers) {

    var sortedList = [];
    var counter = 0;
    var size = listOfNumbers.length;


    while (counter < size) {

        for (var i = 0; i < listOfNumbers.length; i++) {
            var biggest;

            if (i === 0 || listOfNumbers[i] > biggest) {
                biggest = listOfNumbers[i];
            }

        }

        sortedList.push(biggest);
        listOfNumbers.splice(listOfNumbers.indexOf(biggest), 1);
        counter++;
    }

    return (sortedList)
}

