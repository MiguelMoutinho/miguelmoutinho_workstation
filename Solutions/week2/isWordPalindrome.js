
function isWordPalindrome(word) {

    var backwards = word.length - 1;

    var isPalindrome = true;

    for (var i = 0; i < word.length - 1; i++) {
        if (word[i] === word[backwards]) {
            backwards--
        }

        else {
            isPalindrome = false;
            return (isPalindrome);
        }
        return (isPalindrome);
    }

}